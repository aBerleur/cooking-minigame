﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Meal", menuName = "Meal")]
public class Meal : Food {
    public List<Food> Recipe;
}
