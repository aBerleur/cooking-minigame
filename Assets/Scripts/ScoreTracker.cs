﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ScoreTracker : MonoBehaviour {

    public TextMeshProUGUI levelText;
    public int level = 1;
    public int score = 0;
    public int strike = 0;
    private int oldStrike = 0;
    public GameObject[] strikePos;
    public Canvas loseScreen;
    public TextMeshProUGUI scoreText;

    private void Update() {
        levelText.text = "Orders : " + (level - 1);
        if (strike != oldStrike) {
            oldStrike = strike;
            foreach (var obj in strikePos) {
                if (!obj.activeSelf) {
                    obj.SetActive(true);
                    break;
                }
            }

            if (strike == 3) {
                loseScreen.gameObject.SetActive(true);
                scoreText.text = "Score : " + score;
                Time.timeScale = 0;
            }
        }
        //Time.timeScale = 0.1f;
    }

}
