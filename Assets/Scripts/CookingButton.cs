﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CookingButton : MonoBehaviour {

    public Button cookingButton;
    public Menu menu;
    public GameObject foodPrefab;
    public GameObject resultObj;

    void Start () {
        cookingButton.onClick.AddListener(TaskOnClick);
    }

    private void Update() {
        if (menu.result.childCount == 0) {
            resultObj = null;
        }
    }

    void TaskOnClick() {
        Food food = menu.isComplete();
        if (food != null && resultObj == null) {
            if (menu.resultObj != null) {
                Destroy(menu.resultObj);
            }
            resultObj = Instantiate(foodPrefab, menu.result);
            FoodDisplay display = resultObj.GetComponent<FoodDisplay>();
            if (display != null) {
                display.Setup(food);
            }
            

            foreach (var slot in menu.cookingSlots) {
                if (slot.itemObj != null) {
                    Destroy(slot.itemObj);
                }
            }
        }
    }
}
