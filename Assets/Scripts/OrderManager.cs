﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OrderManager : MonoBehaviour {

    public Menu menu;
    public GameObject orderPrefab;
    public Transform gridPosition;
    public ScoreTracker level;
    private int rushHourChance;
    public float rushHourDelay;
    public int orderCount;

    float timer = 0.0f;
    float orderDelay = 0;

    private void Start() {
        level = FindObjectOfType<ScoreTracker>();        
    }

    void Update() {
        timer += Time.deltaTime;
        int seconds = (int)(timer % 60);
        if (timer >= orderDelay ) {
            timer = 0;
            orderDelay = getDelay();
            addOrder();
        }
        if (orderCount == 0) {
            addOrder();
        }
        rushHourChance = (int)Mathf.Pow(2f, (-orderCount + 7f));
        rushHourChance = rushHourChance < 5 ? 5 : rushHourChance;
    }

    private void addOrder() {        
        GameObject order = null;      
        foreach (Transform child in gridPosition) {
            if (child.transform.childCount == 0) {
                order = Instantiate(orderPrefab, child);
                order.transform.parent = child;
                order.transform.position = child.transform.position;
                orderCount++;
                break;
            }
        }
        if (order) {
            int i = Random.Range(0, menu.menu.Length);
            order.GetComponentInChildren<Order>().Setup(menu.menu[i]);
        }
    }

    private float getDelay() {
        float value;
        if (Random.Range(1,100) <= rushHourChance) {
            value = rushHourDelay;
        } else {
            float max = Mathf.Pow(0.99f, (level.level)) * 10;
            float min = Mathf.Pow(0.99f, (level.level)) * 6;
            value = Random.Range(min, max);
        }
        
        return value;
    }
}
