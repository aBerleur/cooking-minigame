﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CookingSlot : MonoBehaviour, IDropHandler {

    public GameObject itemObj;

    public void OnDrop(PointerEventData eventData) {
        if (!itemObj) {
            itemObj = DragHandler.objBeingDragged;
            itemObj.transform.SetParent(transform);
            itemObj.transform.position = transform.position;
        } 
    }
}
