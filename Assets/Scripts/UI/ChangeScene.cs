﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour {

    public string newScene;

    private void Start() {
        GetComponent<Button>().onClick.AddListener(changeScene);
    }

    public void changeScene() {
        SceneManager.LoadScene(newScene);
        Time.timeScale = 1;
    }
}
