﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Order : MonoBehaviour, IDropHandler, IPointerClickHandler {

    public enum orderState {
        INPROGRESS,
        FAILED,
        COMPLETED
    }

    public GameObject ghostFood;
    private GameObject itemObj;
    public Transform orderTable;
    private float timer;
    private float timeLeft;
    private Meal order;
    private orderState state;
    public Slider progressionBar;
    public Image color;
    public Transform orderDetail;
    public OrderManager orderManager;

    public void Setup (Meal order) {
        this.order = order;
        orderManager = FindObjectOfType<OrderManager>();
        GameObject resultObj = Instantiate(ghostFood, transform);
        FoodDisplay display = resultObj.GetComponent<FoodDisplay>();
        if (display != null)
            display.Setup(order);
        float count = Mathf.Log(order.Recipe.Count+2);
        timer = count * Mathf.Pow(0.999f,orderManager.level.level) * 30;
        timeLeft = timer;
        state = orderState.INPROGRESS;
        orderDetail = FindObjectOfType<OrderDetail>().transform.parent.parent.GetChild(1);
    }

    public void OnDrop(PointerEventData eventData) {
        if (!itemObj && state.Equals(orderState.INPROGRESS)) {
            itemObj = DragHandler.objBeingDragged;
            itemObj.transform.SetParent(transform);
            itemObj.transform.position = transform.position;
            if (itemObj.GetComponent<FoodDisplay>().food.name.Equals(order.name)) {
                state = orderState.COMPLETED;
                StartCoroutine("completeAnimation",true);
            }
        }
    }

    void Update() {
        if (itemObj != null) {
            if (itemObj.transform.parent != transform) {
                itemObj = null;
            }
        }
        if (state.Equals(orderState.INPROGRESS)) {
            float percent = timeLeft / timer;
            progressionBar.value = percent;
            if (percent < 0.40) {
                color.color = Color.red;
            } else if (percent < 0.65) {
                color.color = Color.yellow;
            }
            timeLeft -= Time.deltaTime;
        }
        if (timeLeft < 0 && state.Equals(orderState.INPROGRESS)) {
            state = orderState.FAILED;
            StartCoroutine("completeAnimation", false);
        }
    }

    public void OnPointerClick(PointerEventData eventData) {
        orderDetail.parent.GetChild(0).GetChild(0).GetComponent<OrderDetail>().modifyText(order.name + " : ");
        orderDetail.parent.GetChild(0).GetChild(0).GetComponent<OrderDetail>().modifyIcons(order.icon);
        for (int i = 0; i < orderDetail.childCount; i++) {
            
            OrderDetail detail = orderDetail.GetChild(i).GetComponent<OrderDetail>();
            if (i < order.Recipe.Count) {
                detail.modifyText(order.Recipe[i].name);
                detail.modifyIcons(order.Recipe[i].icon);
            } else {
                detail.modifyText("");
                detail.icons.enabled = false;
            }
        }    
    }

    IEnumerator completeAnimation(bool complete) {
        GameObject checkMark = null;
        if (complete) {
            checkMark = (GameObject)Instantiate(Resources.Load("Checked"), transform);
            orderManager.level.score += orderManager.level.level * order.Recipe.Count;
            orderManager.level.level++;            
        } else {
            checkMark = (GameObject)Instantiate(Resources.Load("Failed"), transform);
            orderManager.level.strike++;
        }
        checkMark.transform.SetParent(transform);
        checkMark.transform.position = transform.position;
        
        yield return new WaitForSeconds(2);
        orderManager.orderCount--;
        Destroy(transform.parent.gameObject);
    }
}
