﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FoodDisplay : MonoBehaviour {

    public Food food;
    public Image icon;

    public void Setup (Food food) {
        this.food = food;
        icon.sprite = food.icon;
    }
}
