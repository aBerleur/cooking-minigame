﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Food", menuName = "Food")]
public class Food : ScriptableObject {

    public new string name;
    public Sprite icon;
    
    [HideInInspector]
    public GameObject itemObject;

    public override bool Equals(object obj) {
        if (!(obj is Food)) {
            return false;
        }
        Food food = (Food)obj;
        if (!name.Equals(food.name)) {
            return false;
        }
        return true;
    }
}
