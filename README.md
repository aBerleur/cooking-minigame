# Cook-it

![](demo.gif)

## Description

Cooking game prototype made with unity where the goal is to make meals to satisfy order before the time run out. When the time run out, you take a strike, once you have 3 strikes, you lose.

[The game is playable here](https://aberleur.itch.io/cook-it)

## Dependency

You need [Unity](https://unity.com/) to open the project