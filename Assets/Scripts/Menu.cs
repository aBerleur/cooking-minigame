﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour {

    public Meal[] menu;
    
    public Transform result;
    public GameObject ghostFood;

    public Meal resultMeal;
    public GameObject resultObj;
    [HideInInspector]
    public CookingSlot[] cookingSlots;
    public CookingButton button;
    

    private void Awake() {
        menu = Resources.LoadAll<Meal>("Foods/Meals");
        cookingSlots = FindObjectsOfType<CookingSlot>();
    }

    private void Update() {
        Meal food = isComplete();
        if (food == null && resultObj != null) {
            Destroy(resultObj);
        }
        if (food != null && button.resultObj == null && resultObj == null || food != null && resultMeal != food) {
            if (resultObj != null) {
                Destroy(resultObj);
            }
            resultMeal = food;
            resultObj = Instantiate(ghostFood, result);
            FoodDisplay display = resultObj.GetComponent<FoodDisplay>();
            if (display != null)
                display.Setup(food);
        } 
        if (result.childCount == 0) {
            resultObj = null;
        }
    }

    public Meal isComplete() {
        Meal rightFood = null;
        foreach (var food in menu) {
            HashSet<Food> temp = new HashSet<Food>();
            foreach (var ingredient in food.Recipe) {
                temp.Add(ingredient);
            }
            
            HashSet<Food> temp2 = new HashSet<Food>();
            foreach (var slot in cookingSlots) {
                if (slot.itemObj != null) {
                    temp2.Add(slot.itemObj.GetComponentInChildren<FoodDisplay>().food);
                }
            }

            bool contain = true;
            foreach (var ingredient in temp) {
                if (!temp2.Contains(ingredient)) {
                    contain = false;
                    break;
                }
            }

            if (contain && temp.Count == temp2.Count) {
                if (rightFood == null || food.Recipe.Count > rightFood.Recipe.Count) {
                    rightFood = food;
                }
            }
        }
        return rightFood;
    }
}
