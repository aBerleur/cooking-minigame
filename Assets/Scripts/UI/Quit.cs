﻿using UnityEngine;
using UnityEngine.UI;

public class Quit : MonoBehaviour {

    private void Start() {
        GetComponent<Button>().onClick.AddListener(quit);
    }

    public void quit() {
        Application.Quit();
    }
}
