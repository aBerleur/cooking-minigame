﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GarbageSlot : MonoBehaviour, IDropHandler {

    public GameObject itemObj;

    public void OnDrop(PointerEventData eventData) {

        if (!itemObj) {
            itemObj = DragHandler.objBeingDragged;
            Destroy(itemObj);
        }
    }
}
