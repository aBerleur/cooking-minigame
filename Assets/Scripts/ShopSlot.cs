﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopSlot : MonoBehaviour {

    public Food food;
    public GameObject foodPrefab;
    public Transform foodPool;
    private GameObject itemObj;

    void AddFood() {
        if (food != null) {
            itemObj = Instantiate(foodPrefab, foodPool);
            FoodDisplay display = itemObj.GetComponent<FoodDisplay>();
            if (display != null)
                display.Setup(food);
        }
    }
	
	void Update () {
        if (itemObj != null) {
            if (itemObj.transform.parent != transform) {
                itemObj = null;
            }
        } else {
            AddFood();
        }
    }
}
