﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

    public static GameObject objBeingDragged;

    Vector3 startPosition;
    Transform startParent;

    public static Transform itemDraggerParent;
    CanvasGroup canvasGroup;

    void Start() {
        canvasGroup = GetComponent<CanvasGroup>();
        itemDraggerParent = GameObject.FindGameObjectWithTag("ItemDraggerParent").transform;
    }

    public void OnBeginDrag(PointerEventData eventData) {
        objBeingDragged = gameObject;
        startPosition = transform.position;
        startParent = transform.parent;
        transform.SetParent(itemDraggerParent);
        canvasGroup.blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData) {
        transform.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData) {
        objBeingDragged = null;
        canvasGroup.blocksRaycasts = true;

        if (transform.parent == itemDraggerParent) {
            transform.position = startPosition;
            transform.SetParent(startParent);
        }
    }

    public static Food GetItemBeingDragged() {
        return objBeingDragged.GetComponent<FoodDisplay>().food;
    }
}
