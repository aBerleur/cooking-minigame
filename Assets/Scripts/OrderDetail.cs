﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class OrderDetail : MonoBehaviour {
    
    public TextMeshProUGUI text;
    public Image icons;

    public void modifyText(string text) {
        this.text.text = text;
    }

    public void modifyIcons(Sprite icons) {
        this.icons.sprite = icons;
        this.icons.enabled = true;
    }
}
